call plug#begin()

	Plug 'preservim/nerdtree'
	Plug 'tpope/vim-fugitive'
	Plug 'psliwka/vim-smoothie'

call plug#end()

" Set leader to space
let mapleader = " "

" Implement moving between windows
nnoremap <C-H> <C-W>h
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-L> <C-W>l

" Change cursor shape on terminal (iTerm2)
let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_SR = "\<Esc>]50;CursorShape=2\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"

" Change timout for cursor shape change lag fix
set ttimeoutlen=0

" Implement closing window
nnoremap <leader>q :q<CR>

" Allow backspace for erasing blank new lines
set backspace=indent,eol,start

" Set NERDTree shortcuts
nnoremap <leader>n :NERDTreeToggle<CR>

" Set fugitive-vim shortcuts
cab git Git

" Add line numbers and change their color
set number
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTreeToggleVCS | if argc() > 0 || exists("s:std_in") | wincmd p | NERDTreeFind | wincmd p | endif

