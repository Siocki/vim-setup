# Vim Setup

This repository provides a comprehensive setup for Vim, tailored to enhance development efficiency and workflow.

## Prerequisites

Ensure that Vim is installed on your system. You can verify this by running:

```bash
vim --version
```

If Vim is not installed, you can install it using:

- **For Debian/Ubuntu-based systems:**

  ```bash
  sudo apt install vim
  ```

- **For Red Hat/CentOS-based systems:**

  ```bash
  sudo yum install vim
  ```

- **For macOS:**

  ```bash
  brew install vim
  ```

## Installation

1. **Clone the Repository:**

   ```bash
   git clone https://gitlab.com/Siocki/vim-setup.git ~/.vim
   ```

2. **Navigate to the Directory:**

   ```bash
   cd ~/.vim
   ```

3. **Install Plugins:**

   Open Vim and run the following command to install the plugins:

   ```vim
   :PluginInstall
   ```

## Usage

After completing the installation steps, your Vim environment will be configured with the settings and plugins included in this repository.

## Contributing

Contributions are welcome! Please fork this repository, make your changes, and submit a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Acknowledgments

Special thanks to the contributors of the plugins and resources included in this setup. 
